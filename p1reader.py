import re
import time
import serial
import sys
import logging
from influxdb import InfluxDBClient
from datetime import datetime


class P1Reader:
    def __init__(self, dbclient: InfluxDBClient):
        self.init_com_port()
        self.dbclient = dbclient

    def init_com_port(self):
        """Set COM port config: KAIFA MA105C"""
        ser = serial.Serial()
        ser.baudrate = 115200
        ser.bytesize = serial.SEVENBITS
        ser.parity = serial.PARITY_EVEN
        ser.stopbits = serial.STOPBITS_ONE
        ser.xonxoff = 0
        ser.rtscts = 0
        ser.timeout = 20
        ser.port = "/dev/ttyUSB0"
        try:
            ser.open()
        except:
            print('Failed to open serial port connection')
        self.ser = ser

    @staticmethod
    def strip(x):
        return float(re.search(r"\(.*\)", x)[0].strip('()*kWh'))

    def get_message(self, id):
        return self.strip(''.join([e for e in self.telegram if e.startswith(id)]))

    def get_power_from_telegram(self):
        """Get lines from telelgram that correspond to total en current power usage"""
        return {
            "total_peak": self.get_message('1-0:1.8.1'),
            "total_off": self.get_message('1-0:1.8.2'),
            "current_peak": self.get_message('1-0:1.7.0'),
            "current_off": self.get_message('1-0:2.7.0')
        }

    def read_serial_port(self):
        """Reading the 26 lines in the KAIFA MA105C telegram message"""
        telegram = []
        for _ in range(26):
            p1_raw = self.ser.readline()
            telegram.append(p1_raw.decode())
        self.telegram = telegram

    def write_power_usage(self):
        try:
            self.read_serial_port()
        except:
            logging.warning('Failed to read power usage using serial port')

        power = [
            {
                "measurement": "energy_use",
                "tags": {
                    "meter": "KAIFA_MA105C"
                },
                "time": str(datetime.now()),
                "fields": self.get_power_from_telegram()
            }
        ]
        if not self.dbclient.write_points(power):
            logging.warning('Failed to write power usage to influxDB')


if __name__ == "__main__":
    INTERVAL = 10
    DBNAME = 'energy'
    HOST = 'localhost'
    PORT = 8086
    DEBUG = False

    print('Starting script')
    print('Initializing database client')

    try:
        dbclient = InfluxDBClient(host=HOST, port=PORT)
        print('Connection initialized')
    except:
        print('Failed to connect tot InfluxDB')

    print('Checking if database excist')
    db_names = [e['name'] for e in dbclient.get_list_database()]
    if DBNAME not in db_names:
        logging.warning(f'Created database: {DBNAME}')
        dbclient.create_database(DBNAME)

    print(f'Switching to {DBNAME}')
    dbclient.switch_database(DBNAME)

    print(f'Initializing P1Reader')
    reader = P1Reader(dbclient)

    print(f'Reading power usage with {INTERVAL}s interval')
    while True:
        reader.write_power_usage()
        if DEBUG:
            print(reader.get_power_from_telegram)